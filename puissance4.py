import numpy as np
from copy import deepcopy

class Puissance4:
    def __init__(self):
        colonne = {"stack":0, 0:"_", 1:"_", 2:"_", 3:"_", 4:"_", 5:"_", 6:"_"}
        self.c_list = [colonne.copy() for _ in range(7)]
        self.joueurs = ["●", "○"]

    def place(self, joueur, colonne):
        c = self.c_list[colonne]
        c[c["stack"]] = self.joueurs[joueur]
        if c["stack"] !=6 : c["stack"] += 1

    def est_gagnant(self, joueur):
        _set = np.array([self.joueurs[joueur]]*4)
        _str = lambda x : str(x)[1:-1]
        def in_matrix(M, _bool=True):
            for i in range(7):
                if _bool and (_str(_set) in _str(M[i,:]) or _str(_set) in _str(M[:,i])) : return True
                if _str(_set) in _str(M[range(i, 7), range(0, 7-i)]): return True
                if _str(_set) in _str(M[range(0, 7-i), range(i, 7)]): return True
            return False  
        return in_matrix(self.to_nparray()) or in_matrix(self.to_nparray()[:,::-1], False)

    def gagnant(self):
        string = ""
        gagnant = []
        if self.est_gagnant(0) : string += self.joueurs[0] ; gagnant.append(0)
        if self.est_gagnant(1) : string += " " + self.joueurs[1] ; gagnant.append(1)
        return gagnant, string

    def to_nparray(self):
        M = np.array(list(map(lambda x: list(x.values())[1:], self.c_list)))
        return np.transpose(M)[::-1]

    def to_nparray6x7(self):
        return self.to_nparray()[1:]
        
    def __repr__(self):
        c = self.c_list
        return f"""
        -----------------------
        |[{c[0][5]}][{c[1][5]}][{c[2][5]}][{c[3][5]}][{c[4][5]}][{c[5][5]}][{c[6][5]}]|
        |[{c[0][4]}][{c[1][4]}][{c[2][4]}][{c[3][4]}][{c[4][4]}][{c[5][4]}][{c[6][4]}]|
        |[{c[0][3]}][{c[1][3]}][{c[2][3]}][{c[3][3]}][{c[4][3]}][{c[5][3]}][{c[6][3]}]|
        |[{c[0][2]}][{c[1][2]}][{c[2][2]}][{c[3][2]}][{c[4][2]}][{c[5][2]}][{c[6][2]}]|
        |[{c[0][1]}][{c[1][1]}][{c[2][1]}][{c[3][1]}][{c[4][1]}][{c[5][1]}][{c[6][1]}]|
        |[{c[0][0]}][{c[1][0]}][{c[2][0]}][{c[3][0]}][{c[4][0]}][{c[5][0]}][{c[6][0]}]|
        |---------------------|
        ||0||1||2||3||4||5||6||
        |---------------------|\n
        Gagnant = {self.gagnant()[1]}
        """


    def copy(self):
        fils = Puissance4()
        fils.c_list = deepcopy(self.c_list)
        return fils


# joueur_actuel = 0
# def joueur_suivant():
#     global joueur_actuel
#     joueur_actuel += 1



    




# def generate(jeu):
#     """entrée : une grille;
#        sortie : la liste des grilles fils"""

#     fils=[0]*7
#     for _i in range(7):
#         tmp = jeu.copy()
#         tmp.place(joueur_actuel, _i)
#         fils[_i] = tmp
#     joueur_suivant()
#     return list(map(generate, filter((lambda x: x.gagnant() == ([], "")), fils)))




"""   !!! IMPORTANT !!!   """


""" Il faut vraiment checker les erreurs de typage entre grille et jeu, c'est sur qu'il y en a donc il faut le faire 
    surtout dans l'appel a la fonction couleur
    Vérifier si tout est bien en format ligne, colonnes avec les lignes numérotées de haut en bas et les colonnes de gauche a droite
    (surtout dans la fonction couleur)
    Tout est également en format Matriciel, mais encore une fois tout se règle dans la fonction couleur"""

def couleur (jeu, i, j):
    m = jeu.to_nparray6x7()
    if m[i][j] == "_":
        return "vide"
    elif m[i][j] == "●":
        return "noir"
    elif m[i][j] == "○":
        return "blanc"



def chaine_verticale(jeu, couleur_point, colonne):
    """ entrée : couleur_point : la couleur du joueur a scorer
            : colonne           : la colonne sur laquelle se porte l'étude 
    sortie : le score généré verticalement par cette colonne """

    tamp = 0 # Variable tampon pour compter le score 
    espace = 0 # indique l'espace de jeu jouable entre le dernier pion et le plafond

    if (couleur_point != couleur(jeu, 2, colonne) and couleur(jeu, 2, colonne) != "vide") or (couleur_point != couleur(jeu, 3, colonne) and couleur(jeu, 3, colonne) != "vide") :
        tamp = 0 
    else :
        for a in range (5, -1, -1):
            #Ici cette boucle balaie la ligne en question de bas en haut 
            if tamp >= 4 : #si le combo est de 4 ou plus, on est déja sur un cas de victoire donc le score est infini
                return float("inf")

            #si la couleur du point que l'on regarde est de notre couleur, alors on gagne en score 
            elif couleur_point == couleur (jeu, a, colonne) :
                tamp += 1

            # si la case est vide,l'espace disponible augmente
            elif couleur (jeu, a, colonne) == "vide"  :
                espace += 1

            #sinon si la case n'est pas vide, c'est que ce n'est pas de notre couleur, on a rompu la chaine
            # le score de la colonne est nul et on perd l'espace exploitable s'il n'y en a pas suffisamment
            elif couleur(jeu, a, colonne) != "vide" :
            	if tamp+espace < 4 :
                	tamp = 0
                	espace = 0 
            else : 
                pass

        #On vérifie si l'espace disponible est suffisant pour faire un 4
        if tamp + espace < 4 : 
            tamp = 0

    return tamp


def chaine_horizontale(jeu, couleur_point, ligne) :
    """ entrée : couleur_point : la couleur du joueur a scorer
               : ligne         : la ligne sur laquelle se porte l'étude 
        sortie : le score généré horizontalement par cette ligne """

    tamp = 0 # Variable tampon pour compter le score 
    espace = 0 # indique l'espace de jeu jouable entre deux pions de la ligne 

    #si le point du milieu n'est pas le notre, il est impossible de faire 4 sur la ligne
    if couleur_point != couleur(jeu, ligne, 3) and couleur(jeu, ligne, 3) != "vide" :
        tamp = 0 
    else : 
        for a in range (0, 7):
            #Ici cette boucle balaie la ligne en question de gauche à droite

            #si le combo est de 4 ou plus, on est déja sur un cas de victoire donc le score est infini
            if tamp >= 4 :
                return float("inf")

            #si la couleur du point que l'on regarde est de notre couleur, alors on gagne en score 
            elif couleur_point == couleur (jeu, ligne, a) :
                tamp += 1

            # si la case est vide,l'espace disponible augmente
            elif couleur (jeu, ligne, a) == "vide"  :
                espace += 1

            # si on croise une case de l'autre couleur, mais que l'espace disponible ne permet pas de faire un 4
            # alors l'espace est annulé et le score également
            elif couleur (jeu, ligne, a) != couleur_point :
                if espace+tamp < 4 :
                    tamp = 0
                    espace = 0
        #On revérifie la condition d'espacement a la fin de la boucle 
        if  espace+tamp < 4 :
                tamp = 0
                espace = 0


    #On renvoie le score généré par la ligne 
    return tamp




def chaines_diagonales_decroissantes(jeu, couleur_point):
    """ entrée : grille        : la grille de jeu a analyser
               : couleur_point : la couleur du joueur a scorer
        sortie : le score généré par les diagonales décroissantes """


    #J'ai repris le principe des algos précédents, en voyageant sur les diagonales 
    # il s'agit en effet littéralement de la  même chose que pour le parcours en largeur, mais en diagonale...
    len_diag = 7
    espace = 0
    score = 0

    for départ in range(3): # départ désigne la ligne sur laquelle on commence a compter
        tamp = 0
        len_diag -= 1 #La longueur de la diagonale réduit de 1 lorsque que l'on augmente en ordonnée
        #Point de départ =(départ,0)
        # on parcourt donc comme suit, de droite a gauche et de haut en bas 
        for i,j in zip(range(départ, len_diag + départ), range(len_diag)) :


            #si le combo est de 4 ou plus, on est déja sur un cas de victoire donc le score est infini
            if tamp >= 4 :
                return float("inf")

            #si la couleur du point que l'on regarde est de notre couleur, alors on gagne en score 
            elif couleur_point == couleur (jeu, i, j) :
                tamp += 1

            # si la case est vide,l'espace disponible augmente
            elif couleur (jeu, i, j) == "vide"  :
                espace += 1

            # si on croise une case de l'autre couleur, mais que l'espace disponible ne permet pas de faire un 4
            # alors l'espace est annulé et le score également
            elif couleur (jeu, i, j) != couleur_point :
                if espace+tamp < 4 :
                    tamp = 0
                    espace = 0
        #On revérifie la condition d'espacement a la fin de la boucle 
        if  espace+tamp < 4 :
                tamp = 0
                espace = 0

        # a la fin d'un tour de la grosse boucle, on augmente également le score par tamp
        score += tamp

    len_diag = 7 #On remet la longueur de la diagonale par défaut
    for départ in range(1,3): 
        tamp =0 
        len_diag -= 1 
        #Point de départ =(0,départ)
        for i,j in zip(range(len_diag), range(départ, len_diag + départ)):

            if tamp >= 4 :
                return float("inf")

            elif couleur_point == couleur (jeu, i, j) :
                tamp += 1

            elif couleur (jeu, i, j) == "vide"  :
                espace += 1

            elif couleur (jeu, i, j) != couleur_point :
                if espace+tamp < 4 :
                    tamp = 0
                    espace = 0

        if  espace+tamp < 4 :
                tamp = 0
                espace = 0

        score += tamp

    return score


def chaines_diagonales_croissantes(jeu, couleur_point):
    """ entrée : grille        : la grille de jeu a analyser
           : couleur_point : la couleur du joueur a scorer
    sortie : le score généré par les diagonales décroissantes """

    #ici on ne prendra pas de cas de bases qui seraient trop fastidieux a mettre en place
    # rigoureusement la même chose que pour les diagonales décroissantes, mais avec des diagonales croissantes
    len_diag = 7
    espace = 0
    score = 0

    for départ in range(3): 
        tamp = 0

        len_diag -= 1 
        for i,j in zip(range(5-départ, -1, -1),range(0, len_diag, -1)):
 

            if tamp >= 4 :
                return float("inf")

            elif couleur_point == couleur (jeu, i, j) :
                tamp += 1

            elif couleur (jeu, i, j) == "vide"  :
                espace += 1

            elif couleur (jeu, i, j) != couleur_point :
                if espace+tamp < 4 :
                    tamp = 0
                    espace = 0
        if  espace+tamp < 4 :
                tamp = 0
                espace = 0

        score += tamp

    len_diag = 7
    for départ in range(1,3): 
        len_diag -= 1 

        for i,j in zip(range(5, départ-1, -1),range(départ, 7)):

            if tamp >= 4 :
                return float("inf")

            elif couleur_point == couleur (jeu, i, j) :
                tamp += 1

            elif couleur (jeu, i, j) == "vide"  :
                espace += 1

            elif couleur (jeu, i, j) != couleur_point :
                if espace+tamp < 4 :
                    tamp = 0
                    espace = 0

        if  espace+tamp < 4 :
                tamp = 0
                espace = 0

        score += tamp
        
    return score





def score_final (grille, joueur) :
    # if grille.est_gagnant(joueur) : return 1
    # elif grille.est_gagnant((joueur+1)%2) : return -2

    # else :
    total_1 = chaines_diagonales_decroissantes(grille, "blanc")+chaines_diagonales_croissantes(grille, "blanc") # (a adapter selon la couleur)
    total_2 = chaines_diagonales_decroissantes(grille, "noir")+chaines_diagonales_croissantes(grille, "noir") # idem


    for i in range(6):
        #on somme les scores horizontaux des deux joueurs
        total_1 += chaine_horizontale(grille, "blanc", i )
        total_2 += chaine_horizontale(grille, "noir", i )

    for j in range(7):
        #On somme les scores verticaux des deux joueurs 
        total_1 += chaine_verticale(grille, "blanc", j)
        total_2 += chaine_verticale(grille, "noir", j)

    #On renvoie la différence des deux scores 
    return total_1 - total_2


# def score_final(plateau, joueur):
#     if plateau.est_gagnant(joueur) : return 1
#     elif plateau.est_gagnant((joueur+1)%2) : return -2
#     else: return 0


coups_possibles = lambda plateau : list(filter(lambda x: plateau.c_list[x]["stack"] != 6, range(7))) if plateau.gagnant() == ([], "") else []


def score(plateau, joueur, n):
    """ Renvoie le couple (score attendu si le joueur joue le meilleur coup, le meilleur coup) """
    list_essais = []
    liste_coups = coups_possibles(plateau)
    
    if plateau.est_gagnant(joueur) : return float("inf"), -1
    elif plateau.est_gagnant((joueur+1) %2) : return -float("inf"), -1
    
    elif liste_coups == [] or n == 0 :
        return score_final(plateau, joueur), -1
    
    else:
        for c in liste_coups :
            tmp = plateau.copy()
            tmp.place(joueur, c)
            essai= -score(tmp, ((joueur+1)%2), (n-1))[0] # Attention au signe - : le score pour joueur est l'opposé du score pour l'autre joueur. je ne garde pas la deuxième valeur renvoyée (c'est le coup que devrait jouer l'adversaire si je joue c).

            list_essais.append((essai, c))
            # garder le max des essais ainsi effectués, et garder le coup correspondant, noté meileurCoup ci-dessous
        _max = max(list_essais)
        res = list(filter(lambda x : x[0] == _max[0], list_essais))
        return res[np.random.choice(range(len(res)))]


coup_du_robot = lambda jeu, n: score(jeu, 0, int(n))[1]

def play(jeu):
    """programme une partie d'un joueur contre un ordinateur"""

    print("===========================================================\n\n")
    d = input("Choix de la difficulté : tapez 2(normal) ou 3(difficile) : ")
    print(f"Il s'agit d'un kerman de niveau {d}")
    print(jeu)
    while jeu.gagnant() == ([], ""):
        a = input ("Donner le numéro de la colonne où placer le pion : ")
        jeu.place(1, int(a))
        print(jeu)
        if jeu.gagnant() != ([], "") : break
        print("Le robot Kerman prépare son coup...")
        jeu.place(0, coup_du_robot(jeu, d))
        print(jeu)
    if jeu.gagnant()[0]==[0] :
        print("Trop nul ! Kerman a gagné !")
    else :
        print("Mouais pas mal... Tu as gagné...")
